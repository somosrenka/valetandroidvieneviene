package com.somosrenka.developers.Places;
import android.util.Log;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class PlaceSelectionListeter_ implements PlaceSelectionListener
{
    GoogleMap mMap;
    //GPSTracker gps;
    private ValetPlace lugar_destino=null;
    private boolean isLugarDestinoSelected=false;

    public PlaceSelectionListeter_(GoogleMap googleMap) //, GPSTracker gps)
    {
        Log.i("MapsActivityCarlos", "PlaceSelectionListeter_ CREADO");
        this.mMap = googleMap;
        //this.gps = gps;
        try
            {Log.i("MapsActivityCarlos", mMap.toString());}
        catch(Exception e)
            {Log.i("MapsActivityCarlos", e.toString());}

    }

    @Override
    public void onPlaceSelected(Place place)
    {
        // TODO: Get info about the selected place.
        Log.i("MapsActivityCarlos", "ValetPlace: " + place.getName());
        Log.i("MapsActivityCarlos", "ValetPlace: " + place.getLatLng());
        try
        {
            Log.i("MapsActivityCarlos","OnPlaceSelected");
            Log.i("MapsActivityCarlos", mMap.toString());
            this.DrawPlaceMarker(place);
        }
        catch(Exception e)
            {Log.i("MapsActivityCarlos", e.toString());}
    }

    @Override
    public void onError(Status status)
        {Log.i("MapsActivityCarlos", "An error occurred: " + status);}

    public void DrawPlaceMarker(Place place)
    {
        this.mMap.clear();
        Log.i("MapsActivityCarlos", "DrawPlaceMarker " );
        //LatLng old_location = new LatLng (gps.getLatitude(), gps.getLatitude()) ;
        LatLng latlang_plage = place.getLatLng();
        this.mMap.addMarker(new MarkerOptions().position(latlang_plage).title(place.getName().toString()));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(mexico));
        this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlang_plage, 16)); //MOVIMIENTO ANIMADO
        //GUARDADNO EL MARCADOR DEL LUGAR SELECCIONADO
        if (this.getLugar_destino() ==null)
            {
                this.setLugar_destino(new ValetPlace(place));
                this.isLugarDestinoSelected=true;
            }
        else
            {
                this.getLugar_destino().setPlace(place);
                this.isLugarDestinoSelected=true;
            }
    }

    public ValetPlace getLugar_destino()
        {return lugar_destino;}

    public void setLugar_destino(ValetPlace lugar_destino)
        {this.lugar_destino = lugar_destino;}

    public boolean isLugarDestinoSelected()
        {return isLugarDestinoSelected;}

    public void setLugarDestinoSelected(boolean lugarDestinoSelected)
        {isLugarDestinoSelected = lugarDestinoSelected;}
}
