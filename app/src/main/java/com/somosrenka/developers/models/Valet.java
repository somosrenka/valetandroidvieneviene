package com.somosrenka.developers.models;
import java.io.Serializable;

/**
 * Created by thrashforner on 6/07/16.
 */
public class Valet extends Usuario implements Serializable
{
    public Valet(String email, String token,byte[] password)
    { super(email, token, password); }
}
